"""mrr URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView

import mrr_calculations.views as mrr_calc_views

urlpatterns = [
    url('^', include('django.contrib.auth.urls')),
    url(r'^(?i)$', RedirectView.as_view(url='/login')),
    # Add New CSV
    url(r'^upload_csv/?$', mrr_calc_views.UploadCsvView.as_view()),
    url(r'^update_csv/(?P<tr_id>\d+)/?$', mrr_calc_views.UpdateCsvView.as_view()),
    url(r'^process_data/?$', mrr_calc_views.ProcessView.as_view()),
    # Bar Graphs & Scatter Graphs
    url(r'^bar_analytics/?$', mrr_calc_views.AnalyticsView.as_view()),
    # Accounts
    url(r'^accounts/?$', mrr_calc_views.AccountsView.as_view()),
    url(r'^get_account_mrr/(?P<account_id>\d+)/?$', mrr_calc_views.SingleAccountMrrView.as_view()),
    # Internal Admin Tooling
    url(r'^(?i)admin/?', admin.site.urls),
]
